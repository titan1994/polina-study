# Обучение Полины

glpat-XPW_RyEeyM9NwEFS_SLB

https://gitlab.com/titan1994/polina-study

Собираем полезности тут

## Что освоить немножко:

1. [.gitignore](.gitignore) синтаксис игноров
2. [readme.md](readme.md) синтаксис маркдаунов
3. https://semakin.dev/2020/05/pyenv/ Pyenv
4. pipfile синтаксис
5. Аннотация типов - посмотреть что да - как и использовать. 
6. ctrl+alt+l
7. ctrl+b
8. контекст выполнения программы.

## Лечим терминал

```
python -m pip install pipenv
```

## Основная команда

```
pipenv install
pipenv install -d 
```

## Ссылки

1. https://dictdiffer.readthedocs.io/en/latest/
2. https://proglib.io/p/annotacii-tipov-v-python-vse-chto-nuzhno-znat-za-5-minut-2022-01-30

## Скрины

Настройки отладки, клавиши:

![](.readme_images/cc303041.png)
