'''
На вход вашей программе поступает список целых чисел через пробел (минимум 2)

Напишите программу, которая выводит через пробел суммы чисел стоящих рядом друг с другом.
'''
# Время решения 35 минут

numbers = input().split()
numbers2 = []
for i in range(len(numbers)):
    if i >= len(numbers) - 1:
        break
    else:
        n = int(numbers[int(i)]) + int(numbers[int(i) + 1])
        numbers2.append(n)
        print(n, end=' ')