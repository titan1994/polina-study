"""
Напишите функцию is_prime(num), которая принимает в качестве аргумента натуральное число и
возвращает значение True если число является простым и False в противном случае.
"""


def is_prime(num):
    if num == 1:
        return False
    for i in range(2, num):
        if num % i == 0:
            print(num, i)
            return False  # Почему отрабатывает этот код?
    return True


print("is_prime(1009)", is_prime(1009))
print("is_prime(1)", is_prime(0))
print("is_prime(-1)", is_prime(-1))
print("is_prime(-17)", is_prime(17))
print("is_prime(-10)", is_prime(-10))

# # считываем данные
# n = int(input())
#
# # вызываем функцию
# print(is_prime(n))
#
# '''def number_of_factors(num):  # Мой код
#     counter = 0
#     for i in range(1, num + 1):
#         if num % i == 0:
#             counter += 1
#     return counter
#
# # считываем данные
# n = int(input())
#
# # вызываем функцию
# print(number_of_factors(n))'''
