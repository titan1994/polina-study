def count_sum(**kwargs):
    """Функция принимает любое количество аргументов и возвращает два параметра: список аргументов,
начинающиеся со слова "sum" и сумма значений этих аргументов."""

    summ = 0
    params = []

    for param, val in kwargs.items():
        if param.lower().startswith('sum'):
            if isinstance(val, int):
                summ += val
                params.append(param)
            elif isinstance(val, float):
                summ += val
                params.append(param)
            elif isinstance(val, str):
                numbers = ''

                for k in val:
                    if k.isdigit():
                        numbers += k

                params.append(param)
                summ += int(numbers)
        else:
            continue
    if params:
        return params, summ
    else:
        return 'Нет аргументов, начинающихся со слова "sum"'


print(count_sum(sum=1, summa='2', summ='test223', tes=0))
print(count_sum.__doc__)
