"""
Первый урок Отладка. Проекты и interpreters
"""

from dictdiffer import diff


def tt_dif1(a: int) -> int:
    """
    Просто тест dictdiffer
    """

    first = {
        "title": "hello",
        "fork_count": a,
        "stargazers": ["/users/20", "/users/30"],
        "settings": {
            "assignees": [100, 101, 201],
        }
    }

    second = {
        "title": "hellooo",
        "fork_count": a,
        "stargazers": ["/users/20", "/users/30", "/users/40"],
        "settings": {
            "assignees": [100, 101, 202],
        }
    }

    result = diff(first, second)

    for i in result:
        print(i)

    return 10 + a


if __name__ == '__main__':
    aa = tt_dif1(1)
    print('a:', aa)
