# Функции модуля math

Импортировать можно с помощью онструкции from math import * (в таком случае не придется у
казывать наименование модуля при вызове функции)
![math_functions.png](..%2F..%2F.readme_images%2Fmath_functions.png)
![math_functions2.png](..%2F..%2F.readme_images%2Fmath_functions2.png)
![math_functions3.png](..%2F..%2F.readme_images%2Fmath_functions3.png)